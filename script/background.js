var ENABLED = 'A';
var DISABLED = 'X';
function ChangeIconState() {
	chrome.tabs.query({
		'active' : true,
		'lastFocusedWindow' : true
	}, function (tabs) {
		chrome.browserAction.getBadgeText({}, function (badge) {
			var href = tabs[0].url;
			if (href.indexOf('.se') > 0) {
				if (badge !== ENABLED)
					EnableForPage();
			} else {
				if (badge !== DISABLED)
					DisableForPage();
			}
		});
	});
}
chrome.tabs.onUpdated.addListener(ChangeIconState);
chrome.tabs.onCreated.addListener(ChangeIconState);
chrome.tabs.onActiveChanged.addListener(ChangeIconState);

function DrawGrayscale() {
	var original = new Image();
	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	original.onload = function () {
		context.drawImage(original, 0, 0);
		var imageData = context.getImageData(0, 0, 16, 16);
		var data = imageData.data;
		for (var i = 0, l = data.length; i < l; i++) {
			var brightness = 0.34 * data[i] + 0.5 * data[i + 1] + 0.16 * data[i + 2];
			data[i] = brightness;
			data[i + 1] = brightness;
			data[i + 2] = brightness;
		}
		chrome.browserAction.setIcon({
			imageData : imageData
		});
	};
	original.src = 'img/swe16.png';
};
function EnableForPage() {
	console.log('Popup icon: ENABLED');
	chrome.browserAction.setBadgeText({
		text : ENABLED
	});
	chrome.browserAction.setBadgeBackgroundColor({
		color : '#00FF00'
	});
	chrome.browserAction.setIcon({
		path : 'img/swe16.png'
	});
};
function DisableForPage() {
	console.log('Popup icon: DISABLED');
	chrome.browserAction.setBadgeText({
		text : DISABLED
	});
	chrome.browserAction.setBadgeBackgroundColor({
		color : '#FF0000'
	});
	DrawGrayscale();
};
