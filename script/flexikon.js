$('body').delegate('.gwt-Image', 'click', function(e){
	var link = $(this).parents('td').eq(1).find(".soundimage").parent('a');
	var href = link.attr('href');
	if (href) {
		var object = $(['<object><embed width=20 height=20 src="',href,'" /><param name=loop value=false /></object>'].join(''));
		object.on('load ready', function() {
			$(this).click();
		});
		link.replaceWith(object);
	}
});
$('body').on('load', function() {
	var links = $(this).find(".soundimage").parent('a');
	links.each(function(i, link){
		var href = link.attr('href');
		if (href)
			link.replaceWith($(['<object><embed width=20 height=20 src="',href,'" /><param name=loop value=false></object>'].join('')));
	});
});