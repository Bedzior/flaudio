var windowID = 'SweedenerDictionaryTooltip';

document.body.addEventListener("mouseup", function (event) {
	if (isFL())
		return false;
	setTimeout(HandleFrame, 0, event);
}, true);
document.addEventListener("mousemove", function (event) {
	if (isFL())
		return false;
	RepositionFrame(event);
}, true);
/***
 * Checks if current page is Folkets Lexikon
 */
function isFL() {
	if (window.location.href.indexOf("folkets-lexikon.csc.kth.se") >= 0)
		return true;
	return false;
}
/***
 * Handles frame creation and updating after text selection.
 * Should execute after top frame exit.
 */
function HandleFrame(event) {
	var text = window.getSelection().toString().toLowerCase();
	var div = document.getElementById(windowID);
	var spaces = text.match(/\s/, 'g') || [];
	if (text.length && spaces.length <= 1) {
		var url = ['http://folkets-lexikon.csc.kth.se/folkets/#lookup&', encodeURI(text),'&expandDirectly=t'].join('');
		if (!div) {
			div = CreateWindow(url);
		} else {
			div.src = [url, "&0"].join('');
		}
		div.style.visibility = 'visible';
		RepositionFrame(event);
	} else if (div) {
		div.style.visibility = 'hidden';
	}
};
function CreateWindow(url) {
	var div = document.createElement('iframe');
	div.src = [url, "&0"].join('');
	div.id = windowID;
	div.style.position = 'fixed';
	div.style.width = '500px';
	div.style.height = '8em';
	document.body.appendChild(div);
	return div;
}
function RepositionFrame(event) {
	var div = document.getElementById(windowID);
	if (div && div.style.visibility === 'visible') {
		var bodyWidth = document.body.scrollWidth,
		bodyHeight = window.innerHeight;
		var height = div.clientHeight,
		width = div.clientWidth;
		if (bodyWidth < (event.clientX + width)) {
			div.style.left = (bodyWidth - width) + 'px';
		} else
			div.style.left = (event.clientX + 10) + 'px';
		if (bodyHeight < (event.clientY + height)) {
			div.style.top = (bodyHeight - height) + 'px';
		} else
			div.style.top = (event.clientY + 10) + 'px';
	}
};
