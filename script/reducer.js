if (window !== window.top) {
	console.log('[Frame reduction in progress]');
	window.onload = function () {
		try {
			var body = window.document.body;
			body.className += ' reduced';
			console.log('[Frame reduction complete]');
		} catch (e) {
			console.warn('Frame not reduced!', e);
		}
	};
}
